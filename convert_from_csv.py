"""
CSV Parser/Converter for Vector DBC
"""

import argparse
import csv
import sys

from dbc import Dbc
from message import Message
from dbc_signal import Signal, SIG_UNSIGNED, SIG_SIGNED, SIG_LITTLE_ENDIAN, SIG_BIG_ENDIAN
from enumeration import Enumeration

__author__ = "jtran"
__version__ = "1.0.0"


class CsvParser(object):
    def __init__(self, csv_filepath):
        self._csv_filepath = csv_filepath

        self._lines = []
        with open(self._csv_filepath, "r") as file_obj:
            for line in csv.reader(file_obj):
                self._lines.append(line)

    """
    Public methods
    """
    def convert_to_dbc(self):
        """

        :return: DBC object (DBC)
        """
        new_dbc = Dbc()

        expect_message = False
        expect_signal = False
        for index, line in enumerate(self._lines):
            if line[0].startswith("//") or line[0].startswith("\"//"):
                continue

            if line[0] == "":
                continue

            if "*Message Name" in line[0]:
                expect_message = True
                expect_signal = False
                continue

            if "*Signal Name" in line[0]:
                expect_message = False
                expect_signal = True
                continue

            if len(line[0]) > 0:
                if expect_message:
                    new_message = Message(
                        name=line[0],
                        transmitters=line[1],
                        id=line[2],
                        size=line[3],
                        cycle_time=None if not line[5] else line[5].rstrip("ms")
                    )
                    new_dbc.add_message(new_message=new_message)
                    continue
                elif expect_signal:
                    target_message = new_dbc.messages[-1]
                    new_signal = Signal(
                        name=line[0],
                        start_bit=line[1],
                        size_bit=line[2],
                        endianness=SIG_BIG_ENDIAN if line[3].lower() == "big" else SIG_LITTLE_ENDIAN,
                        signedness=SIG_SIGNED if line[4].lower() == "signed" else SIG_UNSIGNED,
                        factor=line[6],
                        offset=line[7],
                        min=line[8],
                        max=line[9],
                        unit=line[10],
                        receivers=line[11:]
                    )
                    target_message.add_signal(new_signal)

                    if line[5].lower() == "yes":  # Look ahead for enumeration definitions
                        new_enumeration = Enumeration(id=target_message.id, signal_name=target_message.name)
                        temp_index = index
                        temp_index += 1
                        if "*Enum Value" in self._lines[temp_index][5]:
                            temp_index += 1
                            while True:
                                look_ahead_line = self._lines[temp_index]
                                if "".join(look_ahead_line) == "":
                                    break
                                else:
                                    new_enumeration.define(value=look_ahead_line[5], name=look_ahead_line[6])
                                    temp_index += 1

                            new_dbc.add_enumeration(new_enumeration)
        return new_dbc


def get_args():
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument("-i", "--input",
                            type=str,
                            metavar="<file path>",
                            required=True)
    arg_parser.add_argument("-o", "--output",
                            type=str,
                            metavar="<file path>",
                            default="output.dbc")
    return arg_parser.parse_args()


def main():
    args = get_args()
    csv_filepath = args.input
    dbc_filepath = args.output

    csv_parser = CsvParser(csv_filepath=csv_filepath)
    dbc_obj = csv_parser.convert_to_dbc()
    dbc_obj.to_file(output_filepath=dbc_filepath)
    return 0


if __name__ == "__main__":
    sys.exit(main())

    #_csv_parser = CsvParser(csv_filepath="example.csv")
    #_dbc = _csv_parser.convert_to_dbc()
    #print _dbc
