"""
Vector DBC

Enumeration
"""

from collections import OrderedDict

__author__ = "jtran"
__version__ = "1.0.0"


class Enumeration(object):
    def __init__(self, id, signal_name, enum_map=None):
        self._id = int(id)
        self._signal_name = signal_name
        self._enum_map = OrderedDict() if enum_map is None else OrderedDict(enum_map)

    def __iter__(self):
        for key in sorted(self._enum_map.keys()):
            yield key, self._enum_map[key]

    def __str__(self):
        enum_fmt = "{} \"{}\""
        enums = self._enum_map.keys()
        enums = sorted(enums)

        representation = "VAL_ {} {} {};".format(
            self._id,
            self._signal_name,
            " ".join(enum_fmt.format(enum, self._enum_map[enum]) for enum in reversed(enums))
        )
        return representation

    """
    Public methods
    """
    def define(self, value, name):
        self._enum_map[int(value)] = str(name)

    def get_enum(self, target_name):
        ret = None
        for value, name in self._enum_map:
            if target_name == name:
                ret = value
                break
        return ret

    """
    Accessors
    """
    @property
    def id(self):
        return self._id

    @property
    def signal_name(self):
        return self._signal_name


if __name__ == "__main__":  # Test
    _enumeration = Enumeration(id=528, signal_name="DC_DC_State")
    _enumeration.define(value=0, name="Init")
    _enumeration.define(value=1, name="Start")
    _enumeration.define(value=3, name="Idle")
    _enumeration.define(value=5, name="OverTemp")
    _enumeration.define(value=6, name="UnderTemp")
    _enumeration.define(value=7, name="Error")

    for _enum, _name in _enumeration:
        print _enum, _name

    print _enumeration
