"""
Vector DBC

Signal
"""

__author__ = "jtran"
__version__ = "1.0.0"


SIG_LITTLE_ENDIAN = "LITTLE_ENDIAN"
SIG_BIG_ENDIAN = "BIG_ENDIAN"

SIG_UNSIGNED = "UNSIGNED"
SIG_SIGNED = "SIGNED"


class Signal(object):
    def __init__(self, name, start_bit=0, size_bit=0,
                 endianness=SIG_LITTLE_ENDIAN, signedness=SIG_UNSIGNED,
                 factor=1.0, offset=0,
                 min=0.0, max=0.0,
                 unit="",
                 receivers=None):
        """
        :param name: Signal name (str)
        :param start_bit: Start bit (int)
        :param size_bit: Size [bits] (int)
        :param endianness: Endianness (SIG_LITTLE_ENDIAN, SIG_BIG_ENDIAN)
        :param signedness: Signedness (SIG_UNSIGNED, SIG_SIGNED)
        :param factor: Factor (int) or (float)
        :param offset: Offset (int) or (float)
        :param min: Min (int) or (float)
        :param max: Max (int) or (float)
        :param unit: Unit (str)
        :param receivers: Receiver node(s) (str) or (list of str)
        """

        self._name = str(name)
        self._start_bit = int(start_bit)
        self._size_bit = int(size_bit)
        self._endianness = endianness
        self._signedness = signedness
        self._factor = factor
        self._offset = offset
        self._min = min
        self._max = max
        self._unit = str(unit)
        if receivers is not None:
            self._receivers = [receivers] if isinstance(receivers, str) else map(str, receivers)
        else:
            self._receivers = []

        assert self._endianness in [SIG_LITTLE_ENDIAN, SIG_BIG_ENDIAN]
        assert self._signedness in [SIG_UNSIGNED, SIG_SIGNED]

    def __str__(self):
        """
        Example: SG_ Output_Current : 12|12@1- (1.5,100) [-1000|1500] "A" VCU, BMS
        """
        representation = "SG_ {} : {}|{}@{}{} ({},{}) [{}|{}] \"{}\" {}".format(
            self._name,
            self._start_bit,
            self._size_bit,
            (1 if self._endianness == SIG_LITTLE_ENDIAN else 0),
            ("+" if self._signedness == SIG_UNSIGNED else "-"),
            self._min,
            self._max,
            self._factor,
            self._offset,
            self._unit,
            ", ".join(self._receivers)
        )
        return representation

    """
    Public methods
    """

    """
    Private methods
    """

    """
    Accessors
    """
    @property
    def name(self):
        return self._name

    @property
    def start_bit(self):
        return self._start_bit

    @property
    def size_bit(self):
        return self._size_bit

    @property
    def factor(self):
        return self._factor

    @property
    def offset(self):
        return self._offset

    @property
    def min(self):
        return self._min

    @property
    def max(self):
        return self._max

    @property
    def unit(self):
        return self._unit

    @property
    def receivers(self):
        return self._receivers

    """
    Mutators
    """
    @start_bit.setter
    def start_bit(self, new_start_bit):
        self._start_bit = int(new_start_bit)


if __name__ == "__main__":  # Test
    _signal = Signal(name="Output_Current", start_bit=12, size_bit=12, signedness=SIG_UNSIGNED,
                     factor=1.5, offset=100, min=-100, max=1500,
                     unit="A", receivers=["VCU", "BMS"])
    print _signal
