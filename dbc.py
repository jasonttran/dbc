"""
Vector DBC

DBC Representation
"""

import os

from message import Message
from enumeration import Enumeration

__author__ = "jtran"
__version__ = "1.0.0"

DEFAULT_OUTPUT_DBC_FILEPATH = os.path.join(os.path.dirname(__file__), "output.dbc")


class Dbc(object):
    def __init__(self):
        self._messages = []
        self._enumerations = []

    def __str__(self):
        representation = ""
        for message in self._messages:
            representation += "{}\n".format(str(message))
            for signal in message:
                representation += "{}\n".format(str(signal))
            representation += "\n"

        for message in self._messages:
            send_period_fmt = "BA_ \"sendPeriod\" BO_ {} {}"
            representation += "{}\n".format(send_period_fmt.format(message.id, message.cycle_time))

        representation += "\n"

        for enumeration in self._enumerations:
            field_type_fmt = "BA_ \"FieldType\" SG_ {} {} \"{}\""
            representation += "{}\n".format(field_type_fmt.format(enumeration.id, enumeration.signal_name, enumeration.signal_name))
            representation += "{}\n".format(str(enumeration))

        return representation

    """
    Public methods
    """
    def add_message(self, new_message):
        assert isinstance(new_message, Message)
        self._messages.append(new_message)

    def add_enumeration(self, new_enumeration):
        assert isinstance(new_enumeration, Enumeration)
        self._enumerations.append(new_enumeration)

    def to_file(self, output_filepath=DEFAULT_OUTPUT_DBC_FILEPATH):
        with open(output_filepath, "w") as file_obj:
            file_obj.write(str(self))

    """
    Accessors
    """
    @property
    def messages(self):
        return self._messages

    @property
    def enumerations(self):
        return self._enumerations
