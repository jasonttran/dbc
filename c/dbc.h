/*
    @author jtran
    @version 1.0.0
*/

#ifndef DBC_H
#define DBC_H

#include <stdbool.h>
#include <stdint.h>

#define MAX_NAME_SIZE 128U
#define MAX_UNIT_NAME_SIZE 32U
#define MAX_NODE_NAME_SIZE 32U
#define MAX_RECEIVERS_STR_SIZE 128U
#define MAX_SIGNAL_PER_MESSAGE 256U
#define MAX_ENUM_LOOKUP 64U
#define MAX_MESSAGES 256U
#define MAX_SIGNALS 256U
#define MAX_ENUMS 256U


typedef struct {
    uint32_t id;
    char name[MAX_NAME_SIZE];
    uint8_t size;
    char transmitter[MAX_NODE_NAME_SIZE];
    uint16_t cycle_time_ms;
} Message_S;


typedef struct {
    char name[MAX_NAME_SIZE];
    uint8_t start_bit;
    uint8_t size_bits;
    bool is_little_endian;
    bool is_unsigned;
    float factor;
    float offset;
    float min;
    float max;
    char unit[MAX_UNIT_NAME_SIZE];
    char receivers[MAX_RECEIVERS_STR_SIZE];
} Signal_S;


typedef struct {
    // TODO: use hash table or linked list
    uint32_t id;
    char signal_name[MAX_NAME_SIZE];
    int16_t enum_values[MAX_ENUM_LOOKUP];
    char enum_names[MAX_ENUM_LOOKUP][MAX_NAME_SIZE];
    uint16_t length;
} Enumeration_S;


typedef struct {
    Message_S message;
    Signal_S signals[MAX_SIGNALS];
    uint32_t signal_length;
} DBCMessage_S;


typedef struct {
    DBCMessage_S messages[MAX_MESSAGES];
    Enumeration_S enumerations[MAX_ENUMS];
    uint32_t message_length;
    uint32_t enumeration_length;
} DBC_S;


void DBC_init(DBC_S *dbc);
void DBC_add_message(DBC_S *dbc, DBCMessage_S new_message);
void DBC_add_enumeration(DBC_S *dbc, Enumeration_S new_enumeration);
void DBC_define_enumeration(Enumeration_S *enumeration, const char *enum_name, int16_t enum_value);
void DBC_add_signal_to_message(DBCMessage_S *message, Signal_S new_signal);
void DBC_to_file(const char *output_filepath, const DBC_S *dbc);


#endif  // DBC_H
