/*
    @author jtran
    @version 1.0.0
*/

#include "dbc.h"

#include <stdio.h>
#include <string.h>


void DBC_init(DBC_S *dbc)
{
    dbc->message_length = 0U;
    dbc->enumeration_length = 0U;
}


void DBC_add_message(DBC_S *dbc, DBCMessage_S new_message)
{
    dbc->messages[dbc->message_length] = new_message;
    dbc->message_length++;
}


void DBC_add_enumeration(DBC_S *dbc, Enumeration_S new_enumeration)
{
    dbc->enumerations[dbc->enumeration_length] = new_enumeration;
    dbc->enumeration_length++;
}


void DBC_define_enumeration(Enumeration_S *enumeration, const char *enum_name, int16_t enum_value)
{
    (void)strncpy(enumeration->enum_names[enumeration->length], enum_name, strlen(enum_name));
    enumeration->enum_values[enumeration->length] = enum_value;
    enumeration->length++;
}


void DBC_add_signal_to_message(DBCMessage_S *message, Signal_S new_signal)
{
    message->signals[message->signal_length] = new_signal;
    message->signal_length++;
}


void DBC_to_file(const char *output_filepath, const DBC_S *dbc)
{
    // BO_ 528 DC_DC_Converter_Status: 7 DCDC
    // SG_ Output_Current : 12|12@1- (1.5,100) [-1000|1500] "A" VCU, BMS
    // BA_ "sendPeriod" BO_ DC_DC_Converter_Status 10"
    // BA_ "FieldType" SG_ 528 DC_DC_Converter_Status "DC_DC_Converter_Status"
    // VAL_ 528 DC_DC_Converter_Status 7 "Error" 6 "UnderTemp" 5 "OverTemp" 3 "Idle" 1 "Start" 0 "Init";

    FILE *out_fd = fopen(output_filepath, "w");

    for (unsigned int message_index=0U; message_index < dbc->message_length; message_index++) {
        DBCMessage_S dbc_message = dbc->messages[message_index];
        Message_S message = dbc_message.message;
        fprintf(out_fd, "BO_ %d %s: %d %s\n", message.id, message.name, message.size, message.transmitter);

        for (unsigned int signal_index=0U; signal_index < dbc_message.signal_length; signal_index++) {
            Signal_S signal = dbc_message.signals[signal_index];
            fprintf(out_fd, "SG_ %s : %d|%d@%d%c (%0.1f,%0.1f) [%0.1f|%0.1f] \"%s\" %s\n",
                signal.name, signal.start_bit, signal.size_bits,signal.is_little_endian, ((signal.is_unsigned) ? '+' : '-'), 
                signal.min, signal.max, signal.factor, signal.offset,
                signal.unit,
                signal.receivers
            );
        }

        fprintf(out_fd, "\n");
    }

    for (unsigned int message_index=0U; message_index < dbc->message_length; message_index++) {
        DBCMessage_S dbc_message = dbc->messages[message_index];
        Message_S message = dbc_message.message;

        fprintf(out_fd, "BA_ \"sendPeriod\" BO_ %d %d\n", message.id, message.cycle_time_ms);
    }

    fprintf(out_fd, "\n");

    for (unsigned int enumeration_index=0U; enumeration_index < dbc->enumeration_length; enumeration_index++) {
        Enumeration_S enumeration = dbc->enumerations[enumeration_index];
        fprintf(out_fd, "BA_ \"FieldType\" SG_ %d %s \"%s\"\n", enumeration.id, enumeration.signal_name, enumeration.signal_name);
        fprintf(out_fd, "VAL_ %d %s", enumeration.id, enumeration.signal_name);
        for (unsigned int enum_def_index=0U; enum_def_index < enumeration.length; enum_def_index++) {
            fprintf(out_fd, " %d \"%s\"", enumeration.enum_values[enum_def_index], enumeration.enum_names[enum_def_index]);
        }
        fprintf(out_fd, ";\n");
    }


    fclose(out_fd);
}
