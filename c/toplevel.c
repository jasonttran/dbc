/*
    @author jtran
    @version 1.0.0
*/

#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "csv.h"

#include "dbc.h"

#define MAX_CSV_LINES 1024U
#define MAX_CSV_LINE_SIZE 1024U

typedef struct {
    char **elements_array[MAX_CSV_LINES];  // An array of element arrays
    unsigned int length;
} ParsedCsv_S;

static ParsedCsv_S parsed_csv = {
    .length=0U,
};

static DBC_S dbc;

static Message_S decode_elements_message(char **elements);
static Signal_S decode_elements_signal(char **elements);
static bool is_line_valid(char **elements);
static bool is_line_header(char **elements);
static void print_line(char **elements);


int main(int argc, char **argv)
{
    const char *csv_filepath;
    const char *dbc_filepath;
    if (argc <= 2) {
        printf("Please provide the file path of the source CSV file! and the file path of the destination DBC file\n");
        return 1;  // Return early
    }
    else {
        csv_filepath = argv[1];
        dbc_filepath = argv[2];
        printf("Source CSV file path: %s\n", csv_filepath);
        printf("Destination DBC file path: %s\n", dbc_filepath);
    }

    FILE *csv_filehandle = fopen(csv_filepath, "r");

    DBC_init(&dbc);

    // Iterate through the CSV file and read each line
    while (true) {
        char line[MAX_CSV_LINE_SIZE];
        if (fgets(line, MAX_CSV_LINE_SIZE, csv_filehandle) == NULL) {
            break;
        }

        // Parse the CSV line
        parsed_csv.elements_array[parsed_csv.length++] = parse_csv(line);
    }
    fclose(csv_filehandle);

    unsigned int line_index = 0U;
    while (true) {
        if (line_index >= parsed_csv.length) {
            break;
        }

        char **elements = parsed_csv.elements_array[line_index++];

        if (!is_line_valid(elements)) {
            continue;
        }

        else if (strcmp(elements[0U], "*Message Name") == 0) {
            while (true) {
                char **elements = parsed_csv.elements_array[line_index];
                if (is_line_header(elements) || !is_line_valid(elements)) {
                    break;
                }
                else {
                    line_index++;
                }

                Message_S new_message = decode_elements_message(elements);
                DBCMessage_S new_dbc_message = {
                    .message = new_message,
                };
                DBC_add_message(&dbc, new_dbc_message);
            }
        }
        else if (strcmp(elements[0U], "*Signal Name") == 0) {
            while (true) {
                char **elements = parsed_csv.elements_array[line_index];
                if (is_line_header(elements) || !is_line_valid(elements)) {
                    break;
                }
                else {
                    line_index++;
                }
                DBCMessage_S *most_recent_dbc_message_ptr = &(dbc.messages[dbc.message_length-1U]);
                Message_S most_recent_message = (*most_recent_dbc_message_ptr).message;
                Signal_S new_signal = decode_elements_signal(elements);
                DBC_add_signal_to_message(most_recent_dbc_message_ptr, new_signal);

                if (strcmp(elements[5U], "yes") == 0 || strcmp(elements[5U], "Yes") == 0) {
                    Enumeration_S new_enumeration = {
                        .id=most_recent_message.id,
                        .length=0U,
                    };
                    (void)strncpy(new_enumeration.signal_name, most_recent_message.name, strlen(most_recent_message.name));
                    while (true) {
                        line_index++;  // Expect enumeration definitons to be one line below the signal definition
                        char **elements = parsed_csv.elements_array[line_index];
                        if (is_line_header(elements) || (!is_line_valid(elements) && strcmp(elements[5U], "") == 0)) {
                            break;
                        }
                        DBC_define_enumeration(&new_enumeration, elements[6U], atoi(elements[5U]));
                    }

                    DBC_add_enumeration(&dbc, new_enumeration);
                }
            }
        }
    }

    DBC_to_file(dbc_filepath, &dbc);

    return 0;
}


static Message_S decode_elements_message(char **elements)
{
    char cycle_time_ms_str[128U];
    (void)memcpy((void *)cycle_time_ms_str, (void *)elements[5U], strlen(elements[5U])-2U);  // Chop leading "ms" (i.e. "100ms")

    Message_S new_message = {
        .id=atoi(elements[2U]),
        .size=atoi(elements[3U]),
        .cycle_time_ms=atoi(cycle_time_ms_str),
    };

    (void)strncpy(new_message.name, elements[0U], strlen(elements[0U]));
    (void)strncpy(new_message.transmitter, elements[1U], strlen(elements[1U]));

    return new_message;
}


static Signal_S decode_elements_signal(char **elements)
{
    Signal_S new_signal = {
        .start_bit=atoi(elements[1U]),
        .size_bits=atoi(elements[2U]),
        .is_little_endian=(strcmp(elements[3U], "Little") == 0 || strcmp(elements[3U], "little") == 0),
        .is_unsigned=(strcmp(elements[4U], "Unsigned") == 0 || strcmp(elements[4U], "unsigned") == 0),
        .factor=atof(elements[6U]),
        .offset=atof(elements[7U]),
        .min=atof(elements[8U]),
        .max=atof(elements[9U]),
    };

    (void)strncpy(new_signal.name, elements[0U], strlen(elements[0U]));
    (void)strncpy(new_signal.unit, elements[10U], strlen(elements[10U]));
    (void)strncpy(new_signal.receivers, elements[11U], strlen(elements[11U])-1);  // -1 from length to slice newline
    return new_signal;
}


static bool is_line_valid(char **elements)
{
    bool not_valid = (
        strcmp(elements[0U], "") == 0 ||  // Is empty
        strcmp(elements[0U], "\"") == 0 || // Starts with "
        ((elements[0U][0U] == '/') && (elements[0U][1U] == '/'))  // Starts with "//"
    );
    return !not_valid;
}


static bool is_line_header(char **elements)
{
    bool is_header = (
        elements[0U][0U] == '*'  // Starts with "*"
    );
    return is_header;
}


static void print_line(char **elements)
{
    unsigned int element_index = 0U;
    while (true) {
        if (elements[element_index] != NULL) {
            printf("%s ", elements[element_index]);
        }
        else {
            break;
        }
        element_index++;
    }
}
