"""
Vector DBC

Message
"""

from dbc_signal import Signal

__author__ = "jtran"
__version__ = "1.0.0"


class Message(object):
    def __init__(self, id, name, size, transmitters=None, cycle_time=100):
        """
        :param id: Message ID (int)
        :param name: Message name (str)
        :param size: Message size [bytes] (int)
        :param transmitters: Transmitter node(s) (str) or (list of str)
        :param cycle_time: Cycle time in ms (int)
        """
        self._id = int(id)
        self._name = str(name)
        self._size = int(size)
        if transmitters is not None:
            self._transmitters = [transmitters] if isinstance(transmitters, str) else map(str, transmitters)
        else:
            self._transmitters = []
        self._cycle_time = int(cycle_time)
        self._signals = []

    def __str__(self):
        """
        Example: BO_ 528 DC_DC_Converter_Status: 7 DCDC
        """
        representation = "BO_ {} {}: {} {}".format(self._id, self._name, self._size, ", ".join(self._transmitters))
        return representation

    def __iter__(self):
        for signal in self._signals:
            yield signal

    """
    Public methods
    """
    def add_signal(self, new_signal):
        """
        Add a signal to the message
        This method automatically calculates the start bit of the signal
        :return: New signal (Signal)
        """
        assert isinstance(new_signal, Signal)

        start_bit = self._get_next_available_start_bit()

        assert (start_bit + new_signal.size_bit) < (self._size * 8), "Message overflow! Attempted to add: {}".format(new_signal.name)

        new_signal.start_bit = start_bit
        self._signals.append(new_signal)
        return new_signal

    """
    Private methods
    """
    def _get_next_available_start_bit(self):
        available_start_bit = 0
        for signal in self._signals:
            available_start_bit = signal.start_bit + signal.size_bit
        return available_start_bit

    """
    Accessors
    """
    @property
    def id(self):
        return self._id

    @property
    def name(self):
        return self._name

    @property
    def size(self):
        return self._size

    @property
    def transmitters(self):
        return self._transmitters

    @property
    def cycle_time(self):
        return self._cycle_time


if __name__ == "__main__":  # Test
    _message = Message(id=528, name="DC_DC_Converter_Status", size=7, transmitters=["DCDC"])
    print _message

    _message.add_signal(
        Signal(name="Output_Current", size_bit=12,
               factor=1.5, offset=100, min=-100, max=1500,
               unit="A", receivers=["VCU", "BMS"])
    )

    _message.add_signal(
        Signal(name="Output_Voltage", size_bit=12,
               factor=1.5, offset=100, min=-100, max=1500,
               unit="V", receivers=["VCU", "BMS"])
    )

    for _signal in _message:
        print _signal
